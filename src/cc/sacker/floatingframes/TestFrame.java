package cc.sacker.floatingframes;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class TestFrame
{
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(() ->
		{
			JFrame containsFloating = new JFrame();
			containsFloating.setSize(1000, 1000);
			containsFloating.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			containsFloating.setVisible(true);

			JPanel frameContentPane = new JPanel(new GridBagLayout());
			containsFloating.setContentPane(frameContentPane);
			
			JPanel backgroundPanel = new JPanel();
			JButton button = new JButton("I'm in the background! YAY");
			backgroundPanel.add(button);
			FloatingFramesPanel ffp = new FloatingFramesPanel(backgroundPanel);
			
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridheight = 1;
			gbc.gridwidth = 1;
			gbc.gridx = GridBagConstraints.RELATIVE;
			gbc.gridy = GridBagConstraints.RELATIVE;
			gbc.insets = new Insets(0,0,0,0);
			gbc.weightx = 1.0;
			gbc.weighty = 1.0;
			frameContentPane.add(ffp, gbc);
			
			JInternalFrame newFrame = new JInternalFrame();
			newFrame.setSize(200, 200);
			newFrame.setVisible(true);
			
			ffp.addFrame(newFrame);
		});
	}
}
