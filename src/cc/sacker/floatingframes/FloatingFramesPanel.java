package cc.sacker.floatingframes;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLayer;
import javax.swing.JPanel;
import javax.swing.plaf.LayerUI;

public class FloatingFramesPanel extends JPanel
{		
	private static final long serialVersionUID = 7531930167526736410L;
	private static final Color TRANSPARENT = new Color(0,0,0,0);
	private JLayer<JComponent> _jLayer;
	private LayerUI<JComponent> _jlayerLayerUI;
	private JComponent _underlyingComponent;
	private JPanel _desktopPaneWrapper;
	private JDesktopPane _desktopPane;

	public FloatingFramesPanel(JComponent underlyingComponent)
	{
		super(new GridBagLayout());
		_underlyingComponent = underlyingComponent;
		buildComponents();
	}

	private void buildComponents()
	{
		_desktopPaneWrapper = new JPanel(new GridBagLayout());
		_desktopPaneWrapper.setOpaque(false);
		_desktopPaneWrapper.setBackground(TRANSPARENT);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.gridx = GridBagConstraints.RELATIVE;
		gbc.gridy = GridBagConstraints.RELATIVE;
		gbc.insets = new Insets(0, 0, 0, 0);
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		_desktopPane = new JDesktopPane();
		_desktopPane.setOpaque(false);
		_desktopPane.setBackground(TRANSPARENT);

		_jlayerLayerUI = new DefocusingLayerUI<>(_desktopPane);
		_jLayer = new JLayer<>(_underlyingComponent, _jlayerLayerUI);

		_desktopPaneWrapper.add(_desktopPane, gbc);

		_jLayer.setGlassPane(_desktopPaneWrapper);
		_desktopPaneWrapper.setVisible(true);
		
		add(_jLayer, gbc);
	}

	public void addFrame(JInternalFrame newFrame)
	{
		_desktopPane.add(newFrame);
	}

	public JInternalFrame[] getAllFrames()
	{
		return _desktopPane.getAllFrames();
	}

	public void removeFrame(JInternalFrame toRemove)
	{
		_desktopPane.remove(toRemove);
	}
}
