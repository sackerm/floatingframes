package cc.sacker.floatingframes;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;

import javax.swing.JComponent;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLayer;
import javax.swing.plaf.LayerUI;

public class DefocusingLayerUI<V extends Component> extends LayerUI<V>
{

	private static final int NO_EVENT_MASK = 0;

	private JDesktopPane _toDefocus;

	public DefocusingLayerUI(JDesktopPane toDefocus)
	{
		super();
		_toDefocus = toDefocus;
	}

	private static final long serialVersionUID = 5205881454681042244L;

	@Override
	public void installUI(JComponent jComponent)
	{
		super.installUI(jComponent);
		if (jComponent instanceof JLayer)
		{
			((JLayer<?>) jComponent).setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK);
		}
	}

	@Override
	public void uninstallUI(JComponent jComponent)
	{
		super.uninstallUI(jComponent);
		if (jComponent instanceof JLayer)
		{
			((JLayer<?>) jComponent).setLayerEventMask(NO_EVENT_MASK);
		}
	}

	@Override
	public void eventDispatched(AWTEvent awtEvent, JLayer<? extends V> jLayer)
	{
		if (awtEvent.getID() == MouseEvent.MOUSE_PRESSED)
		{
			boolean fromDesktopPane = false;
			Object source = awtEvent.getSource();
			for (Component component : _toDefocus.getComponents())
			{
				if (component == source)
				{
					fromDesktopPane = true;
					break;
				}
			}
			if (!fromDesktopPane)
			{
				for (JInternalFrame jInternalFrame : _toDefocus.getAllFrames())
				{
					try
					{
						jInternalFrame.setSelected(false);
					}
					catch (PropertyVetoException propertyVetoException)
					{
					}
				}
			}
		}
		super.eventDispatched(awtEvent, jLayer);
	}
}
